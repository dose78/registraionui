import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {

  public userReg;

  constructor(private userService: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getUser(this.route.snapshot.params.id);
  }

  getUser(id: number){
    this.userService.getUser(id).subscribe(
      data => {
        this.userReg = data;
      },
      err => console.error(err),
      ()=> console.log('user loaded')
    );
  }

}
