import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

  constructor(private http:HttpClient) { }

  getUsers(){
    let token = localStorage.getItem('access_token');
    return this.http.get('/server/api/v1/users',
      {headers: new HttpHeaders().set('Authorization','Bearer ' + token)}
    );
  }

  getUser(id: number){
    let token = localStorage.getItem('access_token');
    return this.http.get('server/api/v1/users/' + id, 
      {headers: new HttpHeaders().set('Authorization','Bearer ' + token)}
    );
  }

  createUser(user){
    let body = JSON.stringify(user);
    return this.http.post('server/api/v1/users', body, httpOptions);
  }
}
